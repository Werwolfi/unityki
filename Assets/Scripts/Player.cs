﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    private void Awake()
    {
        m_kinematic = new Kinematic(this);
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 newDeltaPosition = new Vector3(0, 0, 0);
        if (Input.GetKey(KeyCode.RightArrow))
        {
            newDeltaPosition += Vector3.right * m_speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            newDeltaPosition += Vector3.left * m_speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            newDeltaPosition += Vector3.forward * m_speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            newDeltaPosition += Vector3.back * m_speed * Time.deltaTime;
        }

        transform.position += newDeltaPosition;

        if (newDeltaPosition.magnitude > 0)
        {
            float currentorientation = -Mathf.Atan2(-newDeltaPosition.x, newDeltaPosition.z);
            m_kinematic.SetRotation(currentorientation * Mathf.Rad2Deg);
        }
    }
}
