﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringBehaviorDynamicSEPERATION : SteeringBehavior
{
    private float m_activationDistance = 0.5f;
    private float m_FOV = 90f;
    private List<Knowledge> m_buddies = new List<Knowledge>();


    public SteeringBehaviorDynamicSEPERATION(string name, Kinematic kinematic, List<Knowledge> buddies) : base(name, kinematic)
    {
        m_buddies = buddies;
    }

    public override Steuerung GetSteuerung()
    {
        Steuerung ergSteuerung = new Steuerung();
        ergSteuerung.m_rotationForce = 0;

        foreach (Knowledge item in m_buddies)
        {
            TargetKinematic target = (TargetKinematic)item;
            if(target != null)
            {
                //Richtungsvektoren der Boids zur eigenen Position
                Vector3 fromMeToTarget = target.GetKinematic().GetPosition() - m_kinematic.GetPosition();
                Vector3 fromTargetToME = m_kinematic.GetPosition() - target.GetKinematic().GetPosition();
                //diejenigen Boids, die innerhalb eines bestimmen Abstands sind
                float distance = fromTargetToME.magnitude;
                if (distance < m_activationDistance && InFOV(m_kinematic.m_speed, fromMeToTarget))
                {
                    float force = m_kinematic.m_maxSteeringForce * (m_activationDistance - distance) / m_activationDistance;
                    ergSteuerung.m_movementForce += fromTargetToME.normalized * force;
                }


            }
        }

        return ergSteuerung;
    }

    private bool InFOV(Vector3 viewOrientation, Vector3 targetOrientation)
    {
        float relativAngle = Vector3.Angle(viewOrientation, targetOrientation);
        if (relativAngle < 0)
        {
            relativAngle *= -1;
        }

        if (relativAngle < (m_FOV))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}