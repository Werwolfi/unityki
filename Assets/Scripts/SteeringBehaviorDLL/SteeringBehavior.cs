﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringBehavior
{
    protected Kinematic m_kinematic;
    protected string m_name;
    float m_spielfeldSize = 5;

    public SteeringBehavior(string name, Kinematic kinematic)
    {
        m_name = name;
        m_kinematic = kinematic;
    }

    public virtual Steuerung GetSteuerung()
    {
        return new Steuerung();
    }

    public string GetName()
    {
        return m_name;
    }

    public bool Update()
    {
        Steuerung ergSteuerung = GetSteuerung();

        ergSteuerung.m_movementForce = Limit(ergSteuerung.m_movementForce, m_kinematic.m_maxSteeringForce);
        m_kinematic.m_speed = Limit(m_kinematic.m_speed, m_kinematic.m_maxSpeed);

        m_kinematic.m_speed += ergSteuerung.m_movementForce * Time.deltaTime;
        if (ergSteuerung.m_rotationForce == 0)
        {
            m_kinematic.m_rotationSpeed = 0;
        }   
        else
        {
            m_kinematic.m_rotationSpeed += ergSteuerung.m_rotationForce * Time.deltaTime;
        }

        BilliardKinematik();

        m_kinematic.TransformPosition(m_kinematic.m_speed * Time.deltaTime);
        m_kinematic.TransformRotation(m_kinematic.m_rotationSpeed * Time.deltaTime);


        //Workaround Boid Orientation

        if (m_kinematic.m_speed.magnitude > 0)
        {
            float currentorientation = -Mathf.Atan2(-m_kinematic.m_speed.x, m_kinematic.m_speed.z);
            m_kinematic.SetRotation(currentorientation * Mathf.Rad2Deg);
        }


        return false;
    }

    private Vector3 Limit(Vector3 vector, float max)
    {
        if (vector.magnitude > max)
        {
            vector = vector.normalized;
            vector *= max;
        }
        return vector;
    }

    public void BilliardKinematik()
    {
        if(m_kinematic.GetPosition().x > m_spielfeldSize || m_kinematic.GetPosition().x < -m_spielfeldSize)
        {
            m_kinematic.m_speed.x *= -1;
        }
        if (m_kinematic.GetPosition().z > m_spielfeldSize || m_kinematic.GetPosition().z < -m_spielfeldSize)
        {
            m_kinematic.m_speed.z *= -1;
        }
    }

}
