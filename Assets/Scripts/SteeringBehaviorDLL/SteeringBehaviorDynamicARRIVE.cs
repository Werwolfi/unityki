﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringBehaviorDynamicARRIVE : SteeringBehavior
{

    float m_stopRadius = 1f;
    float m_timeToTarget = 1f;

    private Knowledge m_target;

    public SteeringBehaviorDynamicARRIVE(string name, Kinematic kinematic, Knowledge target) : base(name, kinematic)
    {
        m_target = target;
    }

    public override Steuerung GetSteuerung()
    {
        Steuerung ergSteuerung = new Steuerung();
        ergSteuerung.m_rotationForce = 0;

        if (m_target == null)
        {
            return ergSteuerung;
        }
        else
        {
            TargetPosition target = (TargetPosition)m_target;

            Vector3 direction = target.GetPosition() - m_kinematic.GetPosition();
            float targetSpeed = 0;

            if (direction.magnitude > m_stopRadius)
            {
                targetSpeed = m_kinematic.m_maxSpeed;
            }
            else
            {
                targetSpeed = 0;
            }

            direction = direction.normalized * targetSpeed;

            ergSteuerung.m_movementForce = direction - m_kinematic.m_speed;
            ergSteuerung.m_movementForce /= m_timeToTarget;
            return ergSteuerung;
        }
    }
}
