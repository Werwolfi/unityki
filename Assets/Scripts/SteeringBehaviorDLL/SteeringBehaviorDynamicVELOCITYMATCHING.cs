﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringBehaviorDynamicVELOCITYMATCHING : SteeringBehavior
{
    private float m_activationDistance = 1f;
    private List<Knowledge> m_buddies = new List<Knowledge>();
    private Vector3 m_averageSpeed = new Vector3(0, 0, 0);


    public SteeringBehaviorDynamicVELOCITYMATCHING(string name, Kinematic kinematic, List<Knowledge> buddies) : base(name, kinematic)
    {
        m_buddies = buddies;
    }

    public override Steuerung GetSteuerung()
    {
        Steuerung ergSteuerung = new Steuerung();
        ergSteuerung.m_rotationForce = 0;

        foreach (Knowledge item in m_buddies)
        {
            TargetKinematic target = (TargetKinematic)item;
            if(target != null)
            {
                //Richtungsvektoren der Boids zur eigenen Position
                Vector3 direction = target.GetKinematic().GetPosition() - m_kinematic.GetPosition();
                //diejenigen Boids, die innerhalb eines bestimmen Abstands sind
                if (direction.magnitude < m_activationDistance)
                {
                    m_averageSpeed = target.GetKinematic().m_speed;
                }

            }
        }
        ergSteuerung.m_movementForce = m_averageSpeed;

        return ergSteuerung;
    }
}