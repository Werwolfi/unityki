﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringBehaviorDynamicWANDER : SteeringBehavior
{
    private float m_offset = 5f; //Distanz zum Kreismittelpunkt
    private float m_radius = 5f;
    private float m_rotationDelta = 0f;
    private float m_maxRotationChange = 10f;
    private Vector3 m_positionOnCircle;


    public SteeringBehaviorDynamicWANDER(string name, Kinematic kinematic) : base(name, kinematic)
    {
    }

    public override Steuerung GetSteuerung()
    {
        Steuerung ergSteuerung = new Steuerung();
        ergSteuerung.m_rotationForce = 0;


        Vector3 posToMid = m_kinematic.m_speed.normalized * m_offset; //Mittlepunkt des Kreises

        m_rotationDelta += (Random.Range(-1f, 1f) * m_offset) * m_maxRotationChange;

        m_positionOnCircle = posToMid + (AngleToVector(m_rotationDelta) * m_radius);

        ergSteuerung.m_movementForce = m_positionOnCircle * m_kinematic.m_maxSteeringForce;

        return ergSteuerung;
    }

    private Vector3 AngleToVector(float angle)
    {
        return new Vector3(Mathf.Sin(Mathf.Deg2Rad * angle), 0, Mathf.Cos(Mathf.Deg2Rad * angle));
    }
}
