﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringBehaviorDynamicFLOCKING : SteeringBehavior
{
    private List<Knowledge> m_buddies = new List<Knowledge>();

    private float m_weightCohersion = 0.5f;
    private float m_weightSeperation = 1f;
    private float m_weightVelocityMatching = 1f;

    private SteeringBehaviorDynamicSEPERATION m_SBSEPERATION; 
    private SteeringBehaviorDynamicVELOCITYMATCHING m_SBVELOCITYMATCHING;
    private SteeringBehaviorDynamicCOHESION m_SBCOHESION;


    public SteeringBehaviorDynamicFLOCKING(string name, Kinematic kinematic, List<KeyValuePair<string, Knowledge>> buddies) : base(name, kinematic)
    {
        foreach (KeyValuePair<string, Knowledge> item in buddies)
        {
            m_buddies.Add(item.Value);
        }

        m_SBVELOCITYMATCHING = new SteeringBehaviorDynamicVELOCITYMATCHING(name, kinematic, m_buddies);
        m_SBSEPERATION = new SteeringBehaviorDynamicSEPERATION(name, kinematic, m_buddies);
        m_SBCOHESION = new SteeringBehaviorDynamicCOHESION(name, kinematic, m_buddies);

    }

    public override Steuerung GetSteuerung()
    {
        Steuerung ergSteuerung = new Steuerung();
        ergSteuerung.m_movementForce = new Vector3(0,0,0);
        ergSteuerung.m_rotationForce = 0f;

        ergSteuerung += m_SBSEPERATION.GetSteuerung() * m_weightSeperation;
        ergSteuerung += m_SBVELOCITYMATCHING.GetSteuerung() * m_weightVelocityMatching;
        ergSteuerung += m_SBCOHESION.GetSteuerung() * m_weightCohersion;

        ergSteuerung.m_movementForce = ergSteuerung.m_movementForce.normalized * m_kinematic.m_maxSteeringForce;


        return ergSteuerung;
    }
}