﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kinematic
{
    private Character m_character;
    //private Vector3 m_position;
    public Vector3 m_speed;
    public float m_maxSpeed;
    public float m_rotationSpeed;
    public float m_maxSteeringForce;

    public Kinematic(Character character)
    {
        m_character = character;
    }

    public Vector3 GetPosition()
    {
        return m_character.transform.position;
    }

    public void TransformPosition(Vector3 deltaPostion)
    {
        deltaPostion.y = 0; //TODO:: Workaround, needs to be calculated better
        m_character.transform.position += deltaPostion;
    }

    public void TransformRotation(float deltaRotation)
    {
        m_character.transform.Rotate(0, deltaRotation, 0);
    }
    public void SetRotation(float rotation)//as degree
    {
        m_character.transform.rotation = Quaternion.Euler(0, rotation, 0);
    }

    public float GetOrientation()
    {
        return m_character.transform.rotation.eulerAngles.y;
    }

}
