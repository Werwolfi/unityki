﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringBehaviorDynamicFLEE : SteeringBehavior
{

    private Knowledge m_target;


    public SteeringBehaviorDynamicFLEE(string name, Kinematic kinematic, Knowledge target) : base(name, kinematic)
    {
        m_target = target;
    }

    public override Steuerung GetSteuerung()
    {
        Steuerung ergSteuerung = new Steuerung();
        ergSteuerung.m_rotationForce = 0;

        if (m_target == null)
        {
            return ergSteuerung;
        }
        else
        {
            TargetPosition target = (TargetPosition)m_target;

            ergSteuerung.m_movementForce = m_kinematic.GetPosition() - target.GetPosition();
            ergSteuerung.m_movementForce = ergSteuerung.m_movementForce.normalized * m_kinematic.m_maxSteeringForce;
            return ergSteuerung;
        }
    }
}
