﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringBehaviorDynamicCOHESION : SteeringBehavior
{
    private float m_activationDistance = 1f;
    private Vector3 m_focus = new Vector3(0, 0, 0);
    private int m_numberOfActivatedBoids;
    private List<Knowledge> m_buddies = new List<Knowledge>();


    public SteeringBehaviorDynamicCOHESION(string name, Kinematic kinematic, List<Knowledge> buddies) : base(name, kinematic)
    {
        m_buddies = buddies;
    }

    public override Steuerung GetSteuerung()
    {
        int numberOfActivatedBoids = 0;
        Steuerung ergSteuerung = new Steuerung();
        ergSteuerung.m_rotationForce = 0;

        foreach (Knowledge item in m_buddies)
        {
            TargetKinematic target = (TargetKinematic)item;
            if(target != null)
            {
                //Richtungsvektoren der Boids zur eigenen Position
                Vector3 direction = m_kinematic.GetPosition() - target.GetKinematic().GetPosition();
                //diejenigen Boids, die innerhalb eines bestimmen Abstands sind
                if (direction.magnitude < m_activationDistance)
                {
                    m_focus += target.GetKinematic().GetPosition();
                    numberOfActivatedBoids++;
                }

            }
        }

        if (numberOfActivatedBoids > 0)
        {
            m_focus /= numberOfActivatedBoids;
            Vector3 direction = m_focus - m_kinematic.GetPosition(); //SEEK
            float force = m_kinematic.m_maxSteeringForce * direction.magnitude / m_activationDistance;
            direction = direction.normalized * force;
            ergSteuerung.m_movementForce = direction;
        }

        return ergSteuerung;
    }
}