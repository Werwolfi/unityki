﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringBehaviorKinematicSEEK : SteeringBehavior
{

    private Knowledge m_target;


    public SteeringBehaviorKinematicSEEK(string name, Kinematic kinematic, Knowledge target) : base(name, kinematic)
    {
        m_target = target;
    }

    public override Steuerung GetSteuerung()
    {
        Steuerung ergSteuerung = new Steuerung();
        ergSteuerung.m_movementForce = new Vector3(0, 0, 0);
        ergSteuerung.m_rotationForce = 0;

        if (m_target == null)
        {
            return ergSteuerung;
        }
        else
        {
            TargetPosition target = (TargetPosition)m_target;

            m_kinematic.m_speed = target.GetPosition() - m_kinematic.GetPosition();
            m_kinematic.m_speed = m_kinematic.m_speed.normalized * m_kinematic.m_maxSpeed;
            m_kinematic.SetRotation(0);
            return ergSteuerung;
        }
    }
}
