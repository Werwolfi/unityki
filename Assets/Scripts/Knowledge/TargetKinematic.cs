﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetKinematic : Knowledge
{
    private Character m_character;

    public TargetKinematic(string name, Character targetKinematic) : base(name)
    {
        m_character = targetKinematic;
    }
    
    public Kinematic GetKinematic()
    {
        return m_character.GetKinematic();
    }
}
