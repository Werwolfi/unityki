﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knowledge
{
    protected string m_name;

    public Knowledge(string name)
    {
        m_name = name;
    }
    

    public string GetName()
    {
        return m_name;
    }
}
