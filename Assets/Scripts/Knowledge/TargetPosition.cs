﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPosition : Knowledge
{
    public Character m_character;

    public TargetPosition(string name, Player targetposition) : base(name)
    {
        m_character = targetposition;
    }

    public Vector3 GetPosition()
    {
        return m_character.transform.position;
    }
}
