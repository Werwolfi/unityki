﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class NPC : Character
{

    protected List<KeyValuePair<string, Knowledge>> m_knowledge = new List<KeyValuePair<string, Knowledge>>();
    protected List<KeyValuePair<string, SteeringBehavior>> m_options = new List<KeyValuePair<string, SteeringBehavior>>();

    private FSM m_fsmFish = new FSM("Fish");
    private float m_energie = 30;
    private float m_timeWandering = 0;

    private void Awake()
    {
        m_kinematic = new Kinematic(this);
    }

    protected override void Start()
    {
        base.Start();
        SteeringBehavior newSteeringBehaviour;

        //KINEMATIC
        //KinematicSEEK
        newSteeringBehaviour = new SteeringBehaviorKinematicSEEK("Option::KinematicSEEK", m_kinematic, m_knowledge.Single(x => x.Key == "TargetPosition::Player").Value);
        m_options.Add(new KeyValuePair<string, SteeringBehavior>(newSteeringBehaviour.GetName(), newSteeringBehaviour));
        //KinematicFLEE
        newSteeringBehaviour = new SteeringBehaviorKinematicFLEE("Option::KinematicFLEE", m_kinematic, m_knowledge.Single(x => x.Key == "TargetPosition::Player").Value);
        m_options.Add(new KeyValuePair<string, SteeringBehavior>(newSteeringBehaviour.GetName(), newSteeringBehaviour));

        //DYNAMIC
        //DynamicSEEK
        newSteeringBehaviour = new SteeringBehaviorDynamicSEEK("Option::DynamicSEEK", m_kinematic, m_knowledge.Single(x => x.Key == "TargetPosition::Player").Value);
        m_options.Add(new KeyValuePair<string, SteeringBehavior>(newSteeringBehaviour.GetName(), newSteeringBehaviour));
        //DynamicFLEE
        newSteeringBehaviour = new SteeringBehaviorDynamicFLEE("Option::DynamicFLEE", m_kinematic, m_knowledge.Single(x => x.Key == "TargetPosition::Player").Value);
        m_options.Add(new KeyValuePair<string, SteeringBehavior>(newSteeringBehaviour.GetName(), newSteeringBehaviour));
        //DynamicARRIVE
        newSteeringBehaviour = new SteeringBehaviorDynamicARRIVE("Option::DynamicARRIVE", m_kinematic, m_knowledge.Single(x => x.Key == "TargetPosition::Player").Value);
        m_options.Add(new KeyValuePair<string, SteeringBehavior>(newSteeringBehaviour.GetName(), newSteeringBehaviour));
        //DynamicWANDER
        newSteeringBehaviour = new SteeringBehaviorDynamicWANDER("Option::DynamicWANDER", m_kinematic);
        m_options.Add(new KeyValuePair<string, SteeringBehavior>(newSteeringBehaviour.GetName(), newSteeringBehaviour));
        //DynamicFLOCKING
        newSteeringBehaviour = new SteeringBehaviorDynamicFLOCKING("Option::DynamicFLOCKING", m_kinematic, m_knowledge.FindAll(x => x.Key == "TargetKinematic::Buddy"));
        m_options.Add(new KeyValuePair<string, SteeringBehavior>(newSteeringBehaviour.GetName(), newSteeringBehaviour));


        m_fsmFish.AddState(new State("WANDER"));
        m_fsmFish.AddState(new State("EAT"));
        m_fsmFish.AddState(new State("GROUP"));

        Action newAction = null;
        newAction = new ActionEAT("EAT", this);
        m_fsmFish.AddTransition("EAT", "WANDER", newAction);

        newAction = new ActionWANDER("WANDER", this);
        m_fsmFish.AddTransition("WANDER", "GROUP", newAction);

        newAction = new ActionGROUP("GROUP", this);
        m_fsmFish.AddTransition("GROUP", "EAT", newAction);

        m_fsmFish.SetStartState("EAT");
    }

    private void Update()
    {
        UpdateKeyboardInput();  

        if (m_currentoption != null)
        {
            m_currentoption.Update();
            transform.position = m_kinematic.GetPosition();
            transform.rotation = Quaternion.Euler(0, m_kinematic.GetOrientation(), 0);
        }

        m_fsmFish.Update();

    }

    public void AddKnowledge(Knowledge newKnowledge)
    {
        m_knowledge.Add(new KeyValuePair<string, Knowledge>(newKnowledge.GetName(), newKnowledge));
    }

    private void UpdateKeyboardInput()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (Input.GetKeyUp(KeyCode.S))
            {
                m_currentoption = m_options.Single(x => x.Key == "Option::KinematicSEEK").Value;
            }
            if (Input.GetKeyUp(KeyCode.F))
            {
                m_currentoption = m_options.Single(x => x.Key == "Option::KinematicFLEE").Value;
            }
        }
        else
        {
            if (Input.GetKeyUp(KeyCode.S))
            {
                m_currentoption = m_options.Single(x => x.Key == "Option::DynamicSEEK").Value;
            }
            if (Input.GetKeyUp(KeyCode.F))
            {
                m_currentoption = m_options.Single(x => x.Key == "Option::DynamicFLEE").Value;
            }
            if (Input.GetKeyUp(KeyCode.A))
            {
                m_currentoption = m_options.Single(x => x.Key == "Option::DynamicARRIVE").Value;
            }
            if (Input.GetKeyUp(KeyCode.W))
            {
                m_currentoption = m_options.Single(x => x.Key == "Option::DynamicWANDER").Value;
            }
            if (Input.GetKeyUp(KeyCode.G))
            {
                m_currentoption = m_options.Single(x => x.Key == "Option::DynamicFLOCKING").Value;
            }
            
        }
    }

    public void SetCurrentOption(string name)
    {
        m_currentoption = m_options.Single(x => x.Key == name).Value;
    }

}
