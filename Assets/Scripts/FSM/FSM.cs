﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM
{
    string m_name;
    State m_currentState = null;
    State m_initialState = null;
    private List<State> m_states = new List<State>();

    public FSM(string name)
    {
        m_name = name;
    }

    public void Update()
    {
        if (m_currentState.Update())
        {
            m_currentState = m_currentState.GetNextState();
            Debug.Log("Switching State to " + m_currentState.GetName());
        }
    }
    public void SetStartState(string initialState)
    {
        m_initialState = getStateByName(initialState); ;
        if (m_currentState == null)
        {
            m_currentState = m_initialState;
        }
    }

    public void AddState(State state)
    {
        m_states.Add(state);
    }

    public State getStateByName(string name)
    {
        return m_states.Find(x => x.GetName() == name);
    }
    public void AddTransition(string from, string to, Action task)
    {
        State state = getStateByName(from);

        state.SetNextSate(getStateByName(to));
        state.SetAction(task);
    }
}
