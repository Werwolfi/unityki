﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State
{
    string m_name;
    Action m_action = null;
    State m_nextState = null;

    public State(string name)
    {
        m_name = name;
    }

    public bool Update()
    {
        return m_action.Update();
    }

    public void SetNextSate(State next)
    {
        m_nextState = next;
    }
    public State GetNextState()
    {
        return m_nextState;
    }

    public void SetAction(Action action)
    {
        m_action = action;
    }

    public string GetName()
    {
        return m_name;
    }
}
