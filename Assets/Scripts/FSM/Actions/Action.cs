﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action
{
    string m_name;
    public Action(string name)
    {
        m_name = name;
    }

    public virtual bool Update() //return true if condition is met
    {
        //do something


        return false;
    }
}
