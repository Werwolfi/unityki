﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionWANDER : Action
{
    string m_name;
    NPC m_npc;

    float m_timer = 0;
    public ActionWANDER(string name, NPC npc) : base(name)
    {
        m_npc = npc;
    }

    public override bool Update() //return true if condition is met
    {
        m_timer += Time.deltaTime;

        if (m_timer < 20){
            m_npc.SetCurrentOption("Option::DynamicWANDER");
            return false;
        }
        //do something
        m_timer = 0;
        return true;
    }
}
