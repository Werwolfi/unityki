﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionEAT :Action
{
    string m_name;
    NPC m_npc;

    float m_timer = 0;
    public ActionEAT(string name, NPC npc) : base(name)
    {
        m_npc = npc;
    }

    public override bool Update() //return true if condition is met
    {
        m_timer += Time.deltaTime;

        if (m_timer < 10){
            m_npc.SetCurrentOption("Option::DynamicARRIVE");
            return false;
        }
        //do something
        m_timer = 0;
        return true;
    }
}
