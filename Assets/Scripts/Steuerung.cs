﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Steuerung
{
    public Vector3 m_movementForce = new Vector3(0, 0, 0);
    public float m_rotationForce = 0;

    public static Steuerung operator *(Steuerung strg, float multiplier)
    {
        strg.m_movementForce *= multiplier;
        strg.m_rotationForce *= multiplier;

        return strg;
    }

    public static Steuerung operator +(Steuerung strg, Steuerung strg2)
    {
        strg.m_movementForce += strg2.m_movementForce;
        strg.m_rotationForce += strg2.m_rotationForce;

        return strg;
    }
}
