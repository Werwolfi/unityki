﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{

    protected float m_speed = 2f;
    protected Kinematic m_kinematic;
    protected SteeringBehavior m_currentoption;

    protected virtual void Start()
    {
        m_kinematic.m_maxSpeed = 0.5f;
        m_kinematic.m_rotationSpeed = 0f;
        m_kinematic.m_maxSteeringForce = 2 * m_kinematic.m_maxSpeed;
    }

    public ref Kinematic GetKinematic()
    {
        return ref m_kinematic;
    }


}
