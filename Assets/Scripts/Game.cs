﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField]
    private int m_numberOfNPC;
    private List<NPC> m_NPC = new List<NPC>();
    private float m_npcScale = 0.25f;

    [SerializeField]
    private GameObject m_characterPrefab;

    private Player m_player;
    private float m_playerScale = 0.5f;


    private float m_spielfeldSize;

    //Materials
    [SerializeField]
    private Material m_playerMaterial;

    [SerializeField]
    private Material m_NPCMaterial;


    void Awake()
    {
        //Determine the Play Area Size to spawn objects inside of it
        m_spielfeldSize = this.GetComponent<Renderer>().bounds.max.x - 1;

        //Initialisiere Player
        GameObject player = Instantiate(m_characterPrefab, new Vector3(0, 0.5f * m_playerScale, 0), Quaternion.identity, this.transform);
        player.transform.localScale = new Vector3(m_playerScale, m_playerScale, m_playerScale);
        m_player = player.AddComponent<Player>();
        player.name = "Player";
        player.GetComponent<Renderer>().material = m_playerMaterial;

        //Initialisiere KI Objekte
        for (int i = 0; i < m_numberOfNPC; i++)
        {

            GameObject newNpc = Instantiate(m_characterPrefab, new Vector3(Random.Range(-m_spielfeldSize, m_spielfeldSize), 0.5f * m_npcScale, Random.Range(-m_spielfeldSize, m_spielfeldSize)), Quaternion.identity, this.transform);
            m_NPC.Add(newNpc.AddComponent<NPC>());
            newNpc.transform.localScale = new Vector3(m_npcScale, m_npcScale, m_npcScale);
            newNpc.name = "NPC";
            newNpc.GetComponent<Renderer>().material = m_NPCMaterial;
        }

        //Jeder NPC kennst den Player
        for (int i = 0; i < m_NPC.Count; i++)
        {
            m_NPC[i].AddKnowledge(new TargetPosition("TargetPosition::Player", m_player));
            m_NPC[i].AddKnowledge(new TargetKinematic("TargetKinematic::Player", m_player));
            //Jeder NPC kennt sein Team
            for (int j = 0; j < m_NPC.Count; j++)
            {
                if (i != j)
                {
                    m_NPC[i].AddKnowledge(new TargetKinematic("TargetKinematic::Buddy", m_NPC[j]));
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
